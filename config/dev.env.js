'use strict'
const merge = require('webpack-merge');
const prodEnv = require('./prod.env');

module.exports = merge(prodEnv, {
	NODE_ENV: '"development"',
	APP_BASE_URL: '"http://127.0.0.1:50001/"',
	API_BASE_URL: '"http://127.0.0.1:50002/api/v1/"',
	AVATAR_URL: '"http://127.0.0.1:50002/public/uploads/"'
});
