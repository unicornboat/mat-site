'use strict';

module.exports = {
	NODE_ENV: '"production"',
	APP_BASE_URL: '"https://matchkun.rioko.studio/"',
	API_BASE_URL: '"https://matchkun.rioko.studio/api/v1/"',
	AVATAR_URL: '"https://files.matchkun.rioko.studio/"'
};
