import 'babel-polyfill';
import 'es6-promise/auto';
import Vue from 'vue';
import Vuex from 'vuex';
import App from './App';
import router from './router';
import global from './mixins/global';

import MatFilters from '@/components/MatFilters';
import MatForm from '@/components/MatForm';
import MatInbox from '@/components/MatInbox';
import MatModal from '@/components/MatModal';
import MatPagination from '@/components/MatPagination';
import MatPanel from '@/components/MatPanel';
import MatProjectBlock from '@/components/MatProjectBlock';
import MatSideMenu from '@/components/MatSideMenu';
import MatTabs from '@/components/MatTabs';
import MatTooltip from '@/components/MatTooltip';
import MatUserEmailNotVerified from '@/components/MatUserEmailNotVerified';
import QuickMessage from '@/components/QuickMessage';
import TabBox from '@/components/TabBox';

require('@/assets/css/reset.css');
require('@/assets/css/font.css');
require('@/assets/css/element.css');
require('@/assets/css/main.css');

Vue.config.productionTip = false;
Vue.use(Vuex);
Vue.mixin(global);
Vue.component('filters', MatFilters);
Vue.component('mat-form', MatForm);
Vue.component('mat-inbox', MatInbox);
Vue.component('mat-modal', MatModal);
Vue.component('mat-pagination', MatPagination);
Vue.component('mat-panel', MatPanel);
Vue.component('mat-project-block', MatProjectBlock);
Vue.component('mat-side-menu', MatSideMenu);
Vue.component('mat-tabs', MatTabs);
Vue.component('mat-tooltip', MatTooltip);
Vue.component('mat-user-email-not-verified', MatUserEmailNotVerified);
Vue.component('quick-message', QuickMessage);
Vue.component('tab-box', TabBox);

const store = new Vuex.Store({
	debug: true,
	state: {
		businessTypes: [],
		keywords: '',
		project: null,
		searchType: '',
		searchIndustry: [],
		token: localStorage.getItem('token'),
		user: null
	}
});

new Vue({
	el: '#app',
	router,
	components: {App},
	store: store,
	template: '<App/>'
})
