import axios from 'axios';
import Vue from 'vue';
import Router from 'vue-router';
import About from '@/pages/About';
import AccountEdit from '@/pages/AccountEdit';
import Accounts from '@/pages/Accounts';
import ChangePassword from '@/pages/ChangePassword';
import Contact from '@/pages/Contact';
import Favourites from '@/pages/Favourites';
import ForgotPassword from '@/pages/ForgotPassword';
import Home from '@/pages/Home';
import Login from '@/pages/Login';
import MyPage from '@/pages/MyPage';
import Messages from '@/pages/Messages';
import Notifications from '@/pages/Notifications';
import PrivacyPolicy from '@/pages/PrivacyPolicy';
import Project from '@/pages/Project';
import ProjectEdit1 from '@/pages/ProjectEdit1';
import ProjectEdit2 from '@/pages/ProjectEdit2';
import ProjectEdit3 from '@/pages/ProjectEdit3';
import Register from '@/pages/Register';
import RegisterConfirm from '@/pages/RegisterConfirm';
import RegisterSuccess from '@/pages/RegisterSuccess';
import ResendConfirmation from '@/pages/ResendConfirmation';
import ResetPassword from '@/pages/ResetPassword';
import SearchProjects from '@/pages/SearchProjects';
import TermsAndConditions from '@/pages/TermsAndConditions';
import Tickets from '@/pages/Tickets';
import NotFound from '@/pages/NotFound';

const document_title = 'MATCHKUN | 日本最大級のオープンイノベーションプラットフォーム';
import { getUserData } from '../mixins/global';

Vue.use(Router);

let router = new Router({
	mode: 'hash',
	scrollBehavior(to, from, savedPosition) {
		if (savedPosition) {
			return savedPosition;
		}
		document.querySelector('main').scrollTo(0,0);
		return {x: 0, y: 0};
	},
	routes: [
		{
			path: '/account/edit',
			name: 'account-edit',
			component: AccountEdit,
			meta: {
				isLoginRequired: true
			}
		},
		{
			path: '/accounts',
			name: 'accounts',
			component: Accounts,
			meta: {
				isLoginRequired: true
			}
		},
		{
			path: '/',
			name: 'home',
			component: Home,
			meta: {
				title: 'MATCHKUN | 日本最大級のオープンイノベーションプラットフォーム'
			}
		},
		{
			path: '/about',
			name: 'about',
			component: About,
			meta: {
				title: 'MATCHKUN | ABOUT US'
			}
		},
		{
			path: '/change-password',
			name: 'change-password',
			component: ChangePassword,
			meta: {
				isLoginRequired: true,
				title: 'MATCHKUN | パスワード変更'
			}
		},
		{
			path: '/contact',
			name: 'contact',
			component: Contact,
			meta: {
				title: 'MATCHKUN | お問合せ'
			}
		},
		{
			path: '/favourites',
			name: 'favourites',
			component: Favourites,
			meta: {
				isLoginRequired: true,
				title: 'MATCHKUN | ABOUT US'
			}
		},
		{
			path: '/forgot-password',
			name: 'forgot-password',
			component: ForgotPassword,
			meta: {
				title: 'MATCHKUN | パスワードをリセット'
			}
		},
		{
			path: '/login',
			name: 'login',
			component: Login,
			meta: {
				title: 'MATCHKUN | ログイン'
			}
		},
		{
			path: '/messages',
			name: 'messages',
			component: Messages,
			meta: {
				isLoginRequired: true,
				title: 'MATCHKUN | メッセージ'
			}
		},
		{
			path: '/my-page',
			name: 'my-page',
			component: MyPage,
			meta: {
				isLoginRequired: true,
				title: 'MATCHKUN | 会員'
			}
		},
		{
			path: '/notifications',
			name: 'notifications',
			component: Notifications,
			meta: {
				isLoginRequired: true,
				title: 'MATCHKUN | お知らせ'
			}
		},
		{
			path: '/privacy-policy',
			name: 'privacy-policy',
			component: PrivacyPolicy,
		},
		{
			path: '/project/:id',
			name: 'project',
			component: Project,
		},
		{
			path: '/project/edit/1',
			name: 'project-edit-1',
			component: ProjectEdit1,
			meta: {
				isLoginRequired: true
			},
		},
		{
			path: '/project/edit/2',
			name: 'project-edit-2',
			component: ProjectEdit2,
			meta: {
				isLoginRequired: true
			},
		},
		{
			path: '/project/edit/3',
			name: 'project-edit-3',
			component: ProjectEdit3,
			meta: {
				isLoginRequired: true
			},
		},
		{
			path: '/register',
			name: 'register',
			component: Register,
			meta: {
				title: 'MATCHKUN | 会員登録'
			}
		},
		{
			path: '/register/confirm/:token',
			name: 'register-confirm',
			component: RegisterConfirm,
			meta: {
				title: 'MATCHKUN | 会員登録完了'
			}
		},
		{
			path: '/register/success',
			name: 'register-success',
			component: RegisterSuccess,
			meta: {
				title: 'MATCHKUN | 会員登録完了'
			}
		},
		{
			path: '/resend-confirmation',
			name: 'resend-confirmation',
			component: ResendConfirmation,
			meta: {
				title: 'MATCHKUN | 確認メールを再送信'
			}
		},
		{
			path: '/password/reset/:token',
			name: 'reset-password',
			component: ResetPassword,
			meta: {
				title: 'MATCHKUN | パスワードを再設定'
			}
		},
		{
			path: '/search/projects',
			name: 'search-projects',
			component: SearchProjects,
			meta: {
				isLoginRequired: true,
				title: 'MATCHKUN | 提携を検討している企業'
			}
		},
		{
			path: '/terms-and-conditions',
			name: 'terms-and-conditions',
			component: TermsAndConditions,
		},
		{
			path: '/tickets',
			name: 'tickets',
			component: Tickets,
			meta: {
				title: 'MATCHKUN | プラン / メッセージチケット'
			}
		},
		{
			path: '*',
			name: 'not-found',
			component: NotFound
		}
	]
});

router.beforeEach((to, from, next) => {
	// Set document title
	document.title = to.meta.title ? to.meta.title : document_title;
	console.log('From "%s" to "%s"', from.name, to.name);

	getUserData()
		.then(user => { // Logged in
			if (isPathAllowedForLoggedInUsers(to.name)) {
				next();
				return;
			}
			next({name: 'my-page'});
			console.log('Navigation is cancelled when logged user trying to access %s', to.name)
		})
		.catch(err => { // Not logged in
			if (!to.meta.isLoginRequired) {
				next();
				return;
			}
			if (to.name !== 'login') next({ name: 'login' });
		});
});

export default router;

const isPathAllowedForLoggedInUsers = function (path) {
	const forbidden_paths = ['forgot-password', 'login', 'register', 'reset-password'];
	return forbidden_paths.indexOf(path) === -1;
};
