import axios from 'axios';
import moment from 'moment';

const timezone_offset = moment().utcOffset();

const httpRequest = function (url, method, data, is_login_required = false, custom_headers) {
	return new Promise(function(resolve, reject) {
		let config = {
			baseURL: process.env.API_BASE_URL,
			headers: {'Access-Control-Allow-Origin': '*'},
			url: url,
			method: method,
			params: {
				timestamp: new Date().getTime()
			}
		};
		if (data) config.data = data;
		if (is_login_required) {
			if (
				localStorage.getItem('token')
				&& localStorage.getItem('token').trim() !== ''
			) {
				config.headers['Authorization'] = 'Bearer ' + localStorage.getItem('token').trim();
			} else {
				reject('ログイン検証に失敗しました');
				return;
			}
		}
		if (custom_headers) {
			for (let key in custom_headers) {
				config.headers[key] = custom_headers[key];
			}
		}

		axios(config)
			.then(function(res) {
				if (res.data.error) {
					reject(res.data.message);
					return;
				}
				resolve(res.data);
			})
			.catch(function (err) {
				console.log(err);
				if (err.message === 'Network Error') {
					reject('ネットワークエラー');
					return;
				}
				reject('エラー');
			});
	});
};

const updateAuthToken = function(token) {
	if (typeof token === 'string' && token.trim() !== '') {
		localStorage.setItem('token', token);
	} else {
		localStorage.clear();
	}
};

var globalUserData = null;

export default {
	data () {
		return {
			avatarUrl: process.env.AVATAR_URL,
		};
	},
	filters: {
		formatDateTime (val) {
			moment.locale('ja');
			return moment(val).utcOffset(timezone_offset).format('MMMDD h:mmA');
		},

		timeAgo (val) {
			moment.locale('ja');
			return moment(val).fromNow();
		},

		truncate (val, max) {
			if (val) {
				if (val.length <= max) return val;
				return val.substr(0, max - 3) + '...';
			}
		}
	},
	methods: {
	}
};

const addFavourite = function(id) {
	return new Promise(function(resolve, reject) {
		httpRequest('user/favourite/' + id, 'post', null, true)
			.then(function() {
				resolve();
			})
			.catch(function(err) {
				reject(err);
			});
	});
};

const forgotPassword = function(data) {
	return new Promise(function(resolve, reject) {
		httpRequest('auth/forgot-password', 'post', data)
			.then(function() {
				resolve();
			})
			.catch(function(err) {
				reject(err);
			});
	});
};

const getFavourites = function(page) {
	return new Promise(function(resolve, reject) {
		httpRequest('user/favourites/' + page, 'get', null, true)
			.then(function(res) {
				resolve(res.data);
			})
			.catch(function(err) {
				reject(err);
			});
	});
};

const getIndustries = function() {
	return new Promise(function(resolve, reject) {
		httpRequest('project/industries', 'get')
			.then(function(res) {
				resolve(res.data);
			})
			.catch(function(err) {
				reject(err);
			});
	});
};

const getMessageList = function() {
	return new Promise(function(resolve, reject) {
		httpRequest('message/list', 'get', null, true)
			.then(function(res) {
				resolve(res.data);
			})
			.catch(function(err) {
				reject(err);
			});
	});
};

/**
 *
 * @param id {String}  The other user's ID
 * @returns  {Promise}
 */
const getMessages = function(id) {
	return new Promise(function(resolve, reject) {
		httpRequest('message/' + id, 'post', null, true)
			.then(function(res) {
				resolve(res.data);
			})
			.catch(function(err) {
				reject(err);
			});
	});
};

const getProjectData = function(id) {
	return new Promise(function(resolve, reject) {
		httpRequest('project/' + id, 'get')
			.then(function(res) {
				resolve(res.data);
			})
			.catch(function(err) {
				reject(err);
			});
	});
};

const getProjectTypes = function() {
	return new Promise(function(resolve, reject) {
		httpRequest('project/types', 'get')
			.then(function(res) {
				resolve(res.data);
			})
			.catch(function(err) {
				reject(err);
			});
	});
};

const getUserData = function() {
	return new Promise(function(resolve, reject) {
		httpRequest('user', 'get', null, true)
			.then(function(res) {
				globalUserData = res.data;
				resolve(res.data);
			})
			.catch(function(err) {
				globalUserData = null;
				reject(err);
			});
	});
};

const login = function(data) {
	return new Promise(function(resolve, reject) {
		httpRequest('auth/login', 'post', data)
			.then(function(res) {
				updateAuthToken(res.token);
				resolve();
			})
			.catch(function(err) {
				reject(err);
			});
	});
};

const logout = function() {
	localStorage.clear();
	globalUserData = null;
	httpRequest('auth/logout', 'post', null, true)
		.catch(function(err) {
			console.log(err);
		});
};

const register = function(data) {
	return new Promise(function(resolve, reject) {
		httpRequest('auth/register', 'post', data)
			.then(function(res) {
				updateAuthToken(res.token);
				resolve();
			})
			.catch(function(err) {
				reject(err.message);
			});
	});
};

const removeFavourite = function(id) {
	return new Promise(function(resolve, reject) {
		httpRequest('user/favourite/' + id, 'delete', null, true)
			.then(function() {
				resolve();
			})
			.catch(function(err) {
				reject(err);
			});
	});
};

const removeIndustry = function(id) {
	return new Promise(function(resolve, reject) {
		if (!id) {
			reject('Something went wrong. Please try again.');
			return;
		}
		httpRequest(
			'project/industry/' + id,
			'delete',
			null,
			true
		)
			.then(function(res) {
				resolve(res);
			})
			.catch(function(err) {
				reject(err);
			});
	});
};

const resendConfirmationEmail = function () {
	return new Promise(function(resolve, reject) {
		httpRequest(
			'auth/resend',
			'post',
			null,
			true
		)
			.then(function(res) {
				resolve(res);
			})
			.catch(function(err) {
				reject(err);
			});
	});
};

const resetPasswordByToken = function(data) {
	return new Promise(function(resolve, reject) {
		httpRequest('auth/reset-password-by-token', 'post', data)
			.then(function() {
				resolve();
			})
			.catch(function(err) {
				reject(err);
			});
	});
};

const saveUserData = function(user_data) {
	return new Promise(function(resolve, reject) {
		httpRequest('user/update', 'post', user_data, true)
			.then(function(data) {
				resolve(data);
			})
			.catch(function(err) {
				reject(err);
			});
	});
};

const search = function(keywords, search_type, industries, filters, page, order_by) {
	return new Promise(function(resolve, reject) {
		httpRequest('search', 'post', {
			keywords: keywords,
			search_type: search_type,
			industries: industries,
			filters: filters,
			page: parseInt(page) > 0 ? parseInt(page) : 1,
			order_by: order_by
		})
			.then(function(res) {
				resolve(res.data);
			})
			.catch(function(err) {
				reject(err);
			});
	});
};

const sendMessage = function(receiver_id, project_id, content) {
	return new Promise(function(resolve, reject) {
		httpRequest('message/add', 'post', {
			receiver_id: receiver_id,
			project_id: project_id,
			content: content,
			time: new Date().toISOString()
		}, true)
			.then(function(res) {
				resolve(res.data);
			})
			.catch(function(err) {
				reject(err);
			});
	});
};

const sendMessageToCustomerService = function(name, company, email, content) {
	return new Promise(function(resolve, reject) {
		httpRequest('auth/message-customer-service', 'post', {
			name: name,
			company: company,
			email: email,
			content: content
		})
			.then(function() {
				resolve();
			})
			.catch(function(err) {
				reject(err);
			});
	});
};

const toggleProjectPublicity = function(val) {
	return new Promise(function(resolve, reject) {
		httpRequest(
			'project/publicity',
			'post',
			null,
			true
		)
			.then(function(res) {
				resolve(res);
			})
			.catch(function(err) {
				reject(err);
			});
	});
};

const updateIndustry = function(ids) {
	return new Promise(function(resolve, reject) {
		httpRequest('project/industries', 'post', {ids: ids}, true)
			.then(function(res) {
				resolve(res.data);
			}).catch(function(err) {
				reject(err);
			});
	});
};

const updatePassword = function(data) {
	return new Promise(function(resolve, reject) {
		httpRequest('user/password', 'post', data, true)
			.then(function(res) {
				resolve(res.data);
			})
			.catch(function(err) {
				reject(err);
			});
	});
};

const updateProjectData = function(id, data) {
	return new Promise(function(resolve, reject) {
		httpRequest('project/' + id, 'post', data, true)
			.then(function(res) {
				resolve(res.data);
			})
			.catch(function(err) {
				reject(err);
			});
	});
};

const uploadProjectAvatar = function(file) {
	return new Promise(function(resolve, reject) {
		let data = new FormData();
		data.append('file', file);
		httpRequest(
			'upload/project/avatar',
			'post',
			data,
			true,
			{'Content-Type': 'multipart/form-data'}
		)
			.then(function(res) {
				resolve(res.data);
			})
			.catch(function(err) {
				reject(err);
			});
	});
};

const uploadUserAvatar = function(file) {
	return new Promise(function(resolve, reject) {
		let data = new FormData();
		data.append('file', file);
		httpRequest(
			'upload/user/avatar',
			'post',
			data,
			true,
			{'Content-Type': 'multipart/form-data'}
		)
			.then(function(res) {
				resolve(res.data);
			})
			.catch(function(err) {
				reject(err);
			});
	});
};

const verifyEmailToken = function (token) {
	return new Promise(function(resolve, reject) {
		httpRequest( 'auth/verify-email',  'post', {token: token})
			.then(function() {
				resolve();
			})
			.catch(function(err) {
				reject(err);
			});
	});
};

export {
	addFavourite,
	forgotPassword,
	getFavourites,
	getIndustries,
	getMessageList,
	getMessages,
	getProjectData,
	getProjectTypes,
	getUserData,
	login,
	logout,
	register,
	removeFavourite,
	removeIndustry,
	resendConfirmationEmail,
	resetPasswordByToken,
	saveUserData,
	search,
	sendMessage,
	sendMessageToCustomerService,
	toggleProjectPublicity,
	updateIndustry,
	updatePassword,
	updateProjectData,
	uploadProjectAvatar,
	uploadUserAvatar,
	verifyEmailToken
};
